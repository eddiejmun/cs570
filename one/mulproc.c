/*
 This program will fork two child processes running the two programs generated in programming zero in parallel.
 Hint: You mant need to use fork(), exec() family, wait(), exit(), getpid() and etc ...
 
 Requirements:
 
 1) Exactly two child processes are created, one to run testspecial program and the other is to run testalphabet program;
 2) When a program starts to run, print a message to the output screen showing which process (with PID) is running which program, for example:
    "CHILD <16741> process is executing testalphabet program!"
    
 3) When a program is done, the process exits and at the same time, a message should be print to the output screen showing which  process (with PID) is done with the program, for example:
    "CHILD <16741> process has done with testalphabet program !"
    
 4) The messages should match the real execution orders, i.e. when the testspecial program starts/ends, the right message should be print out. So you need to figure out how to get the starting/ending time of each 
 process.
   
   
 The expected screen print out should be similar as follows:
 
 CHILD <16741> process is executing testalphabet program!
 CHILD <16742> process is executing testspecial program!
, -> 745668
. -> 798072
... ...
s
CHILD <16742> process has done with testspecial program !
a -> 2973036
b -> 556908
... ...

CHILD <16741> process has done with testalphabet program !
 */
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>


int main(void) {
    
    
    pid_t child_a, child_b;
    
    int pid1, pid2 = 0;
    int status;
    char *args[]={"./testalphabet","./testspecial", NULL};

    
    //creates 1st child
    child_a = fork();
    //process of the 1st child
    if (child_a == 0) {
        pid1 = getpid();
        printf("CHILD <PID: %d> process is executing testalphabet program!\n",pid1);
        //idles the 1st child by 1 second
        sleep(1);
        //calls the executable file called ./testalphabet
        execv(args[0],args);
    } else {
        //creates 2nd child
        child_b = fork();
        //the process of the 2nd child
        if (child_b == 0) {
            pid2 = getpid();
            printf("CHILD <PID: %d> process is executing testspecial program!\n",pid2);
            //calls the executable file calles ./testspecial
            execv(args[1],args);
            
        } else {
            //waits till 2nd child is finished then prints out message
            waitpid(child_b,NULL, 0);
             //gets the Parent PID and then increments by 2 to get the child 2 PID
            int pids2 = getpid();
            pids2 = pids2 + 2;
            printf("CHILD <PID: %d> process has finished with testspecial program!\n", pids2);
            
        }
    }
    //waits till 2st child is finished then prints out message
    waitpid(child_a,NULL, 0);
    //gets the Parent PID and then increments by 1 to get the child 1 PID
    int pids1 = getpid();
    pids1 = pids1 + 1;
    printf("CHILD <PID: %d> process has finished with testalphabet program!\n", pids1);
    



}
