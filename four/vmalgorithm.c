/*
 *  Implementation of FIFO and LRU page replacement algorithm
 *  Please add appropriate level of comments in this file 
 */

#include "vmalgorithm.h"


/* Generate an access pattern
 * Example: 3, 2, 2, 1, 1  indicates the page 3, 2, 2, 1, 1 in order
 */
void generateAccessPattern()
{
   int i;
   srand(time(0));
   accessPattern = (int *)malloc( sizeof(int) * AccessPatternLength);   
   printf("The randomized Access Pattern: ");
   for(i=0; i< AccessPatternLength; i++)
   {
		     accessPattern[i] = rand() % ReferenceSZ;
        printf("%d ", accessPattern[i]);       
   }
   printf("\n");
}

/*
 * Initialize the parameters of simulated memory
 */
void initializePageFrame()
{
   int i;
   memory.PageFrameList = (int *)malloc( sizeof(int)* FrameNR );    // dynamic allocated FrameNR frames to be used in memory
   memory.nextToReplaced =0;          // indicate the new frame to be replaced as 0
   for(i=0; i< FrameNR; i++)
   {
			memory.PageFrameList[i] = -1;  // initialization to -1 indicating all frames are unused 
   }

}

// Print the pages loaded in memory
void printPageFrame()
{
   int i;
   for(i=0; i< FrameNR; i++)
   {
			printf("%2d ",memory.PageFrameList[i]);       
   }
   printf("\n");
}


/*
 *  Print the access pattern in order
 */
void printAccessPattern()
{
   int i;
   printf("The Same Access Pattern: ");
   for(i=0; i< AccessPatternLength; i++)
   {
        printf("%d ", accessPattern[i]);       
   }
   printf("\n");

}


/*
 * Return: number of the page fault using FIFO page replacement algorithm
 */
int FIFO()
{
  int pagefaults = 0;
  int ispresent = 0;
  int full = 0;

   
  //iterates through all of the values in accessPattern
  for (int i = 0; i < AccessPatternLength; i++) { 
    ispresent = 0;
    full = 0;
    //checks to see if the current accessPattern value is in the page frame
    for (int j = 0; j < FrameNR; j++) {
      if (accessPattern[i] == memory.PageFrameList[j]) {
        ispresent = 1;
      }
    }
    //if it is present, then print the page frame (no page fault)
    if (ispresent == 1) {
      printPageFrame();
    }
    //if it is not present then check if there are any available spots in the page frame
    if (ispresent == 0) {
      for (int j = 0; j < FrameNR; j++) {
        if (memory.PageFrameList[j] == -1) {
          full++;
        }
      }
      //if there are available spots in the page frame then put the current accessPattern value in the first available spot
      if (1 <= full <=  3) {
        for (int j = 0; j < FrameNR; j++) {
          if (memory.PageFrameList[j] == -1) {
            memory.PageFrameList[j] = accessPattern[i];
            pagefaults++;
            printPageFrame();
            break;
          }
        }
      }
      //if there are no available spots, replace current accessPattern value with the nextToReplaced index
      if (full == 0) {
        memory.PageFrameList[memory.nextToReplaced] = accessPattern[i];
        memory.nextToReplaced++;
        //once the nextToReplaced index has reached the number of frames, set it back to 0 to restart
        if (memory.nextToReplaced == FrameNR) {
          memory.nextToReplaced = 0;
        }
        //incrememnts page faults and then prints out the page frame
        pagefaults++;
        printPageFrame();
      }

      
    }

  }
  //returns the number of page faults
  return pagefaults;



   
   
}



/*
 * Return: number of the page fault using LRU page replacement algorithm
 */

int LRU()
{
  int first = 0;
  int isfirst = 0;
    //TODO: fill the code to impelement LRU replacement algorithm 
  int pagefaults = 0;
  int ispresent = 0;
  int full = 0;
  //iterates through all of the values in accessPattern
  for (int i = 0; i < AccessPatternLength; i++) { 
    ispresent = 0;
    full = 0;
    //iterates through the number of frames
    for (int j = 0; j < FrameNR; j++) {
      //if the current accessPattern value equals a value inside the page frame then signal that it is present
      if (accessPattern[i] == memory.PageFrameList[j]) {
        ispresent = 1;
      }
    }
    //if the current accessPattern is present print the page frame (no page fault)
    if (ispresent == 1) {
      printPageFrame();
    }
    //if the current accessPatter is not present then do more steps
    if (ispresent == 0) {
      //iterate through the number of frames
      for (int j = 0; j < FrameNR; j++) {
        //checks to see if there are any empty spots
        if (memory.PageFrameList[j] == -1) {
          full++;
        }
      }
      //if there are some empty spots put the current accessPattern in the first available spot
      if (1 <= full <=  3) {
        for (int j = 0; j < FrameNR; j++) {
          if (memory.PageFrameList[j] == -1) {
            memory.PageFrameList[j] = accessPattern[i];
            pagefaults++;
            printPageFrame();
            break;
          }
        }
      }
      //if there are no empty spots, it will call the findLRU() function to figure out which page frame to replace
      if (full == 0) {
        memory.nextToReplaced = findLRU(i); 
        memory.PageFrameList[memory.nextToReplaced] = accessPattern[i];
        pagefaults++;
        printPageFrame();
      
      }

      
    }

  }
  //returns the number of page faults
  return pagefaults;



}

int findLRU(int index) {
  //gets the previous accessPattern value to start checking for the LRU
  int n = index-1;
  int spot;
  int nums[FrameNR];
  int k = 0;
  int status = 0;
  int status2 = 0;
  //fills the nums[] with -5 acting as random placeholders
  for (int i = 0; i < FrameNR; i++) {
    nums[i] = -5;
  }
  //iterate through until there are no more accessPattern values to check
  while (n >= 0) {
    status2 = 0;
    //checks each of the page frame values
    for(int i = 0; i < FrameNR; i++) {
      //if the accessPattern value is present in the page frame...
      if (memory.PageFrameList[i] == accessPattern[n]) {
        //...check if it is already been accounted for
        for (int j = 0; j <FrameNR; j++) {
          //if it is already been accounted for then signal 
          if (nums[j] == accessPattern[n]) {
            status2++;
          }
        }
        //if it wasnt already accounted for then add it to nums[] to keep track
        if (status2 == 0) {
          nums[k] = accessPattern[n];
          status++;
          k++;
        } 
      } 
    }
    //move on to the next accessPatern value
    n--;
    //if the last accessPatern value is accounted for then it is the LRU
    if (status == FrameNR) {
      break;
    } 
  }
  n++;
  //checks to see what in the page frame it should be replacing
  for (int i =0; i < FrameNR; i++) {
    if (memory.PageFrameList[i] == accessPattern[n]) {
      spot = i;
    }
  }
  //returns the index spot of where it should be replaced
  return spot;


  
}



