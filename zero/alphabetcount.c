/*
 * alphabetcount.c - this file implements the alphabetlettercount function.
 */

#include <dirent.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include "count.h"


/**
  The alphabetlettercount function counts the frequency of each alphabet letter (a-z, case insensitive) in all the .txt files under
  directory of the given path and write the results to a file named as filetowrite.
  
  Input: 
      path - a pointer to a char string [a character array] specifying the path of the directory; and
      filetowrite - a pointer to a char string [a character array] specifying the file where results should be written in.
      alphabetfreq - a pointer to a long array storing the frequency of each alphabet letter from a - z:
      alphabetfreq[0]: the frequency of 'a'
      alphabetfreq[1]: the frequency of 'b'
         ... ...
      alphabetfreq[25]:the frequency of 'z'

  
  Output: a new file named as filetowrite with the frequency of each alphabet letter written in
  
  Steps recommended to finish the function:
  1) Find all the files ending with .txt and store in filelist.
  2) Read all files in the filelist one by one and count the frequency of each alphabet letter only (a - z). The array 
  long alphabetfreq[] always has the up-to-date frequencies of alphabet letters counted so far. If the letter is upper case, convert
  it to lower case first. 
  3) Write the result in the output file: filetowrite in following format: 
  
     letter -> frequency
     
     example:
     a -> 200
     b -> 101
     ... ...
     
  Assumption:  
  1) You can assume there is no sub-directory under the given path so you don't have to search the files 
  recursively.    
  2) Only .txt files are counted and other types of files should be ignored.
  
*/
void alphabetlettercount(char *path, char *filetowrite, long alphabetfreq[])
{
	
    DIR *dir;
    struct dirent *in_file;
    FILE *entry_file;
    dir = opendir(path);
    char c;
    int charactercount = 0;
    entry_file = fopen(filepath, "r");
            //if there is no file with the filepath name then prompt an error message
            if (entry_file == NULL)
            {
                printf("Cannot open file \n");
                exit(0);
            }
            c = fgetc(entry_file);
            while (c != EOF) {
              printf("%d",c);
            }
    
    while ((in_file=readdir(dir)) != NULL) {
        //ignores the files in the directory that are "." and ".."
        if(!strcmp (in_file->d_name, "."))
        {
            continue;
        }
        if(!strcmp (in_file->d_name, ".."))
        {
            continue;
        }
        char filepath[256] = "../data/";
        int istextfile = 0;
        //puts the file name at the end of file path
        char *name = in_file->d_name;
        //finds the length of the file name
        int length = strlen(name);
        int index = length + 8;
        //appends the file name to the end of the file path
        strcat(filepath,in_file->d_name);
        //checks if it is a .txt extension
        if ('t' == filepath[index-1] && 'x' == filepath[index-2] && 't' == filepath[index-3]) {
            
            istextfile = 1;
            entry_file = fopen(filepath, "r");
            //if there is no file with the filepath name then prompt an error message
            if (entry_file == NULL)
            {
                printf("Cannot open file \n");
                exit(0);
            }
            c = fgetc(entry_file);
            while (c != EOF)
            {
                charactercount++;
                //checks and counts the number of each character a-z and A-Z
                if (65 <= (int)c && (int)c <= 90) {
                    int ascii = (int)c - 65;
                    alphabetfreq[ascii]++;
                }
                if (97 <= (int)c && (int)c <= 122) {
                    int ascii2 = (int)c - 97;
                    alphabetfreq[ascii2]++;
                }
                c = fgetc(entry_file);
                
            }
            char allfiles[charactercount];
            
            fclose(entry_file);
            //clears the name of filepath to be used again for new files
            memset(filepath, 0, sizeof(filepath));
            
        }
        
        
        
    }
    //This creates a new .txt file that has the results printed in the file
    //Creates the file in the filetowrite path
    FILE *fp = NULL;
    fp = fopen(filetowrite ,"a");
    if(fp != NULL) {
        for(int i = 0; i < 26; i++) {
            fprintf(fp,"%c -> %d\n", (char)(i+97), alphabetfreq[i]);
        }
    }
    
    
		
	
}
