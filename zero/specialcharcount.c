/*
 * specialcharcount.c - this file implements the specialcharcount function.
 */


#include <dirent.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include "count.h"

void specialcharcount(char *path, char *filetowrite, long charfreq[])
{

    DIR *dir;
    struct dirent *in_file;
    FILE *entry_file;
    dir = opendir(path);
    char c;
    int charactercount = 0;
    
    while ((in_file=readdir(dir)) != NULL) {
        if(!strcmp (in_file->d_name, "."))
        {
            continue;
        }
        if(!strcmp (in_file->d_name, ".."))
        {
            continue;
        }
        char filepath[256] = "../data/";
        int istextfile = 0;
        //puts the file name at the end of file path
        char *name = in_file->d_name;
        //finds the length of the file name
        int length = strlen(name);
        int index = length + 8;
        //appends the file name to the end of the file path
        strcat(filepath,in_file->d_name);
        //checks if it is a .txt extension
        if ('t' == filepath[index-1] && 'x' == filepath[index-2] && 't' == filepath[index-3]) {
            
            istextfile = 1;
            entry_file = fopen(filepath, "r");
            if (entry_file == NULL)
            {
                printf("Cannot open file \n");
                exit(0);
            }
            c = fgetc(entry_file);
            while (c != EOF)
            {
                charactercount++;
                //checks and counts the number of each special character
                if (c == ',') {
                    charfreq[0]++;
                }
                if (c == '.') {
                    charfreq[1]++;
                }
                if (c == ':') {
                    charfreq[2]++;
                }
                if (c == ';') {
                    charfreq[3]++;
                }
                if (c == '!') {
                    charfreq[4]++;
                }

                c = fgetc(entry_file);
                
            }
            char allfiles[charactercount];
            
            fclose(entry_file);
            memset(filepath, 0, sizeof(filepath));
            
        }
        
        
        
    }
    FILE *fp = NULL;
    fp = fopen(filetowrite ,"a");
    if(fp != NULL) {
        fprintf(fp,", -> %d\n", charfreq[0]);
        fprintf(fp,". -> %d\n", charfreq[1]);
        fprintf(fp,": -> %d\n", charfreq[2]);
        fprintf(fp,"; -> %d\n", charfreq[3]);
        fprintf(fp,"! -> %d\n", charfreq[4]);
    }
    
		
}
/**
  The specialcharcount function counts the frequency of the following 5 special characters:
  ','   '.'   ':'    ';'    '!'
   
  in all the .txt files under directory of the given path and write the results to a file named as filetowrite.
  
  Input: 
  
      path - a pointer to a char string [a character array] specifying the path of the directory; and
      filetowrite - a pointer to a char string [a character array] specifying the file where results should be written in.
      alphabetfreq - a pointer to a long array storing the frequency of the above 5 special characters
      
      charfreq[0]: the frequency of ','
      charfreq[1]: the frequency of '.'
      charfreq[2]: the frequency of ':'
      charfreq[3]: the frequency of ';'
      charfreq[4]: the frequency of '!'

  
  Output: a new file named as filetowrite with the frequency of the above special characters written in
  
  Steps recommended to finish the function:
  1) Find all the files ending with .txt and store in filelist.
  2) Read all files in the filelist one by one and count the frequency of each alphabet letter only (a - z). The array 
  long alphabetfreq[] always has the up-to-date frequencies of alphabet letters counted so far. If the letter is upper case, convert
  it to lower case first. 
  3) Write the result in the output file: filetowrite in following format: 
  
     character -> frequency
     
     example:
     , -> 20
     . -> 11
     : -> 20
     ; -> 11
     ! -> 12     
     
  Assumption:  
  1) You can assume there is no sub-directory under the given path so you don't have to search the files 
  recursively.    
  2) Only .txt files are counted and other types of files should be ignored.
  
*/

