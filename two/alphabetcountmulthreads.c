/*
 * alphabetcountmulthreads.c - this file implements the alphabetcountmulthreads function.
 */

#include <stdio.h> 
#include "count.h"
#include <stdio.h>
#include <dirent.h>
#include <dirent.h>
#include <stdio.h>
#include <math.h>  
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>


/**
  The alphabetcountmulthreads function counts the frequency of each alphabet letter (a-z, case insensitive) in all the .txt files under
  directory of the given path and write the results to a file named as filetowrite. Different with programming assignment#0, you need to implement the program using mutithreading.
  
  Input: 
      path - a pointer to a char string [a character array] specifying the path of the directory; and
      filetowrite - a pointer to a char string [a character array] specifying the file where results should be written in.
      alphabetfreq - a pointer to a long array storing the frequency of each alphabet letter from a - z, which should be already up-to-date;
      num_threads - number of the threads running in parallel to process the files
      
       
  Output: a new file named as filetowrite with the frequency of each alphabet letter written in
  
  Requirements:
1)	Multiple threads are expected to run in parallel to share the workload, i.e. suppose 3 threads to process 30 files, then each thread should process 10 files;
2)	When a thread is created, a message should be print out showing which files this thread will process, for example:
Thread id = 274237184 starts processing files with index from 0 to 10!
3)	When a file is being processed, a message should be print out showing which thread (thread_id = xxx) is processing this file, for example:
Thread id = 265844480 is processing file input_11.txt
4)	When a thread is done with its workload, a message should be print out showing which files this thread has done with work, for example:
      Thread id = 274237184 is done !
5)	The array: long alphabetfreq[ ] should always be up-to-date, i.e. it always has the result of all the threads counted so far.  [You may need to use mutexes to protect this critical region.]


You should have the screen printing should be similar as follows:

 Thread id = 274237184 starts processing files with index from 0 to 10!
 Thread id = 265844480 starts processing files with index from 11 to 22!
 Thread id = 257451776 starts processing files with index from 23 to 31!

 Thread id = 265844480 is processing file input_11.txt
 Thread id = 257451776 is processing file input_22.txt
 Thread id = 274237184 is processing file input_00.txt
  � � 

 Thread id = 274237184 is done !
 Thread id = 265844480 is done !
 Thread id = 257451776 is done !

 The results are counted as follows:
 a -> 2973036
 b -> 556908
 c -> 765864
 �  � 
*/

long freq[26] = {0};

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

//struct of parameters for pthread function
typedef struct args1{
    char* path1;
    struct webData* data;
    int tid;
    int rem;
    int status;
    int numthreads;
    int n;
}args1;

//struct of filenames
struct webData {
    char web_names [255];           
};

void* thread_funtion(void* arg) 
{
  //give every thread a struct of parameters and initialize variables
  args1 *arg_struct = (args1*) arg;
  int n = arg_struct->n;
  int remainder = arg_struct->rem;
  int numthreads = arg_struct->numthreads;
  int status = arg_struct->status;
  char filepath[256] = {0};
  int i = 0;
  //checks if it is last thread, if it is increment by remainder to get remaining .txt

  DIR *dir;
  struct dirent *in_file;
  FILE *entry_file;

 
      //grab file of # of elements dependent on thread (array[n])
      for (i = 0; i < n; i++) {
        struct webData *temp = arg_struct->data + i;
        char c;
        //open "../data" directory
        dir = opendir(filepath);
        char filepath[256] = {0};
        strcat(filepath,arg_struct->path1);
        strcat(filepath,"/");
        strcat(filepath,temp);
        //open the .txt file
        entry_file = fopen(filepath, "r");
  
        pthread_mutex_lock(&mutex);
        printf("Thread id = %d is processing file %s\n",arg_struct->tid,temp);

            //if there is no file with the filepath name then prompt an error message
            if (entry_file == NULL)
            {
                printf("Cannot open file \n");
            } 
            c = fgetc(entry_file);
            while (c != EOF)
            {

                //checks and counts the number of each character a-z and A-Z
                if (65 <= (int)c && (int)c <= 90) {
                    int ascii = (int)c - 65;

                    freq[ascii]++;



                }
                if (97 <= (int)c && (int)c <= 122) {
                    int ascii2 = (int)c - 97;

                    freq[ascii2]++;
                    pthread_mutex_unlock(&mutex);
                }
                c = fgetc(entry_file);

               
                
            }
            pthread_mutex_unlock(&mutex);

           memset(filepath, 0, sizeof(filepath));


            
    }

    pthread_exit(NULL);


}


void alphabetcountmulthreads(char *path, char *filetowrite, long alphabetfreq[], int num_threads)
{
if (num_threads < 2) {
  exit(1);
}
    DIR *dir;
    struct dirent *in_file;
    FILE *entry_file;
    dir = opendir(path);
    char c;
    int file_count = 0;
    int index1 = 0;
    struct webData wData[500];

    
    while ((in_file=readdir(dir)) != NULL) {
        //ignores the files in the directory that are "." and ".."
        if(!strcmp (in_file->d_name, "."))
        {
            continue;
        }
        if(!strcmp (in_file->d_name, ".."))
        {
            continue;
        }

        char filepath[256] = {0};
        //puts the file name at the end of file path
        char *name = in_file->d_name;
        //finds the length of the file name
        int length = strlen(name);
        int index = length + 8;
        strcat(filepath, path);
        strcat(filepath,"/");
        //appends the file name to the end of the file path
        strcat(filepath,in_file->d_name);
        //checks if it is a .txt extension
        if ('t' == filepath[index-1] && 'x' == filepath[index-2] && 't' == filepath[index-3]) {
            file_count++;


            //clears the name of filepath to be used again for new files
            memset(filepath, 0, sizeof(filepath));
            
        }
    }
    //rewinds the directory stream to back to the beginning
    rewinddir(dir);

    char *filesList[file_count];
    while ((in_file=readdir(dir)) != NULL) {
        //ignores the files in the directory that are "." and ".."
        if(!strcmp (in_file->d_name, "."))
        {
            continue;
        }
        if(!strcmp (in_file->d_name, ".."))
        {
            continue;
        }

        char filepath[256] = {0};
        strcat(filepath,path);
        strcat(filepath,"/");
        int len = strlen(filepath);
        int istextfile = 0;
        //puts the file name at the end of file path
        char *name = in_file->d_name;
        //finds the length of the file name
        int length = strlen(name);
        int index = length + 8;
        //appends the file name to the end of the file path
        strcat(filepath,in_file->d_name);
        //checks if it is a .txt extension
        if ('t' == filepath[index-1] && 'x' == filepath[index-2] && 't' == filepath[index-3]) {
            filesList[index1] = (char*) malloc (strlen(filepath)+1);
            strncpy (filesList[index1],filepath, strlen(filepath) );
            strcpy(wData[index1].web_names, in_file->d_name);
            index1++;

            //clears the name of filepath to be used again for new files
            memset(filepath, 0, sizeof(filepath));
            
        }
    }

    closedir(dir);


  //creates an array to store the number of files to process in each thread
  int array[num_threads]; 
  int equalload;
  int remainder;
  int pnt;
  //if # of files is equally divisible by 3, all threads have equal workload

  for(int i = num_threads; i > 0; i--)
    {

        equalload = (int)ceil((double)file_count / (double)i);
        array[num_threads-i] = equalload;
        file_count -= equalload;
    }


  
 
  // Launch threads
  pthread_t tids[num_threads];
  args1 thread_param[num_threads];
  int counter = 0;
  int endcounter;
  int i;
  pnt = 0;
  // creates N number of threads (dependant on user)
  for (i = 0; i < num_threads; i++) { 
    endcounter = counter + array[i] -1;
    struct args1 args;
    //sets the number of elements in thread
    thread_param[i].n = array[i];
    //sets the remainder to thread
    thread_param[i].rem = remainder;
    //sets the pointer to the array specific to the thread (hence the * i)
    thread_param[i].numthreads = num_threads;
    if (i == 0) {
      thread_param[i].data = wData + 0;
    } else {
      pnt = pnt + array[i-1];
      thread_param[i].data = wData + pnt;
     
    }
    //status to check if it is the last thread
    thread_param[i].status = i;
    thread_param[i].path1 = path;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    printf("Thread id = %u starts processing files with index from %d to %d!\n", tids[i],counter,endcounter);
    //creates the thread and points it to thread_function taking in the pointer to the struct parameter
    pthread_create(&tids[i], &attr, thread_funtion, thread_param + i);
    thread_param[i].tid = tids[i];



    counter = counter + array[i];
  }



  // Wait until thread is done its work
  for (int i = 0; i < num_threads; i++) {
    pthread_join(tids[i], NULL);
    printf("Thread id = %u is done !\n",tids[i]);

  }
  //copies the frequencies to alphabetfreq[]
  for (int i = 0; i < 26; i++) {
    alphabetfreq[i] = freq[i];
  }


    //prints out the result to filetowrite in a .txt file
    FILE *fp;
    fp = fopen(filetowrite ,"a");
    if(fp != NULL) {
        for(int i = 0; i < 26; i++) {
            fprintf(fp,"%c -> %d\n", (char)(i+97), alphabetfreq[i]);
        }
    }


}

		
	

